const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");


function matchesPerYear() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        matches.push(datarow)
    })
        .on("end", () => {
            let resultMatchesObject = matches.reduce((accu,matches)=>{
                if (matches.season in accu) {
                    accu[matches.season] += 1;
                } else {
                    accu[matches.season] = 1;
                }
                return accu;
            },{})
            
            fs.writeFile(
                path.join(__dirname, "../public/output/1-matches-per-year.json"),
                JSON.stringify(resultMatchesObject),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}

matchesPerYear();
