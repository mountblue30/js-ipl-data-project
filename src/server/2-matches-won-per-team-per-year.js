const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function matchesWonPerTeam() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({
        columns: true
    })).on('data', function (datarow) {
        matches.push(datarow);
    }).on("end", () => {
        let matchesWonPerTeam = matches.reduce((accu, match) => {
            if (accu[match.season] === undefined) {
                accu[match.season] = { [match.winner]: 0 }
            }
            if (match.winner in accu[match.season]) {
                accu[match.season][match.winner] += 1
            } else {
                accu[match.season][match.winner] = 1

            }
            return accu;
        }, {})
        fs.writeFile(
            path.join(__dirname,
                "../public/output/2-matches-won-per-team-per-year.json"),
            JSON.stringify(matchesWonPerTeam),
            (error) => {
                if (error !== null) {
                    console.log(error);
                }
            }
        )
    })
}


matchesWonPerTeam()