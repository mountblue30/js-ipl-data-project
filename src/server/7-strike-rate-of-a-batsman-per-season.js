const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function strikeRate(player) {
    let seasonMatchID = {};
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarowMatches) {
        seasonMatchID[datarowMatches.id] = datarowMatches.season
    }).on('end', () => {
        let deliveries = [];
        fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarowDeliveries) {
            deliveries.push(datarowDeliveries);
        }).on('end', () => {
            strikeRateOfBatsman = deliveries.reduce((accu,delivery)=>{
                if (delivery.batsman === player) {
                    if (accu[seasonMatchID[delivery.match_id]] === undefined) {
                        accu[seasonMatchID[delivery.match_id]] = { playerName: player, totalRuns: 0, totalBallsFaced: 0, strikeRate :0};
                    }
                    if (delivery.extra_runs === "0") {
                        accu[seasonMatchID[delivery.match_id]]["totalRuns"] += +delivery.total_runs;
                        accu[seasonMatchID[delivery.match_id]]["totalBallsFaced"] += 1;
                        accu[seasonMatchID[delivery.match_id]]["strikeRate"] = +((accu[seasonMatchID[delivery.match_id]]["totalRuns"] * 100) / accu[seasonMatchID[delivery.match_id]]["totalBallsFaced"]).toFixed(3);

                    } else {
                        if (delivery.noball_runs !== "0" || delivery.bye_runs !== "0" || delivery.legbye_runs !== "0") {
                            accu[seasonMatchID[delivery.match_id]]["totalBallsFaced"] += 1;
                            accu[seasonMatchID[delivery.match_id]]["totalRuns"] += +delivery.total_runs - delivery.extra_runs;
                            accu[seasonMatchID[delivery.match_id]]["strikeRate"] = +((accu[seasonMatchID[delivery.match_id]]["totalRuns"] * 100) / accu[seasonMatchID[delivery.match_id]]["totalBallsFaced"]).toFixed(3);
                        }
                        if (delivery.penalty_runs !== "0") {
                            accu[seasonMatchID[delivery.match_id]]["totalRuns"] += delivery.total_runs - delivery.penalty_runs;
                            accu[seasonMatchID[delivery.match_id]]["strikeRate"] = +((accu[seasonMatchID[delivery.match_id]]["totalRuns"] * 100) / accu[seasonMatchID[delivery.match_id]]["totalBallsFaced"]).toFixed(3);
                        }
                    }
                }
                return accu;
            },{})          
            fs.writeFile(
                path.join(__dirname, "../public/output/7-strike-rate-of-a-batsman-per-season.json"),
                JSON.stringify(strikeRateOfBatsman),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
    })
}

strikeRate("PA Patel");

