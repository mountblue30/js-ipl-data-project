const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function PlayerOfTheMatch() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarowMatches) {
        matches.push(datarowMatches);
    }).on('end', () => {
        let playerOfTheMatchPerSeason = matches.reduce((accu,match)=>{
            if (match.season in accu) {
                if (match.player_of_match in accu[match.season]) {
                    accu[match.season][match.player_of_match] += 1;
                } else {
                    accu[match.season][match.player_of_match] = 1;
                }
            } else {
                accu[match.season] = {};
                accu[match.season][match.player_of_match] = 1;
            }
            return accu;
        },{})
        
        let resultObject = Object.entries(playerOfTheMatchPerSeason).reduce((accu,[year,players]) => {
            let maxValue = Math.max(...Object.values(playerOfTheMatchPerSeason[year]));
            accu[year] = [];
            Object.entries(players).reduce((accu,[player,playerOfTheMatchCount]) => {
                if (playerOfTheMatchCount === maxValue) {
                    accu[year].push({ name: player, playerOfTheMatchCount: maxValue })
                }
                return accu;
            },accu)
            return accu;
        },{})
        fs.writeFile(
            path.join(__dirname, "../public/output/6-highest-number-of-Player-of-the-match.json"),
            JSON.stringify(resultObject),
            (error) => {
                if (error !== null) {
                    console.log(error);
                }
            }
        )
    })
}

PlayerOfTheMatch();
