const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");


function mostDismissals(batsman, bowler) {
    let playerDismissedDetails = [];
    fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        if (datarow.player_dismissed !== "" && datarow.noball_runs === "0") {
            playerDismissedDetails.push(datarow);
        }
    })
        .on("end", () => {
            let playerDismissed = playerDismissedDetails.reduce((accu,match)=>{
                if (accu[match.player_dismissed] === undefined) {
                    accu[match.player_dismissed] = {};
                    accu[match.player_dismissed][match.bowler] = 1;
                } else {
                    if (match.bowler in accu[match.player_dismissed]) {
                        accu[match.player_dismissed][match.bowler] += 1;
                    } else {
                        accu[match.player_dismissed][match.bowler] = 1;
                    }
                }
                return accu;
            },{})
            let resultObject = {};
            if (batsman !== undefined && bowler !== undefined) {
                resultObject[batsman] = {};
                resultObject[batsman]["bowler"] = bowler;
                resultObject[batsman]["dismissalCount"] = playerDismissed[batsman][bowler];
                
            }else if(batsman !== undefined && bowler === undefined){
                resultObject[batsman] = playerDismissed[batsman];
            } else{
                resultObject =Object.entries(playerDismissed).reduce((accu,[player,bowlers]) => {
                    let maxValue = Math.max(...Object.values(playerDismissed[player]));
                    accu[player] = [];
                    Object.entries(bowlers).reduce((accu,[bowler,dismissalCount]) => {
                        if (dismissalCount  === maxValue) {
                            accu[player].push({ bowlerName: bowler, dismissedCount: maxValue })
                        }
                        return accu;
                    },accu) 
                    return accu;
                },{})
            }
            fs.writeFile(
                path.join(__dirname, "../public/output/8-most-dismissals-of-a-player-by-another.json"),
                JSON.stringify(resultObject),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}



mostDismissals("DA Warner", "BW Hilfenhaus");



