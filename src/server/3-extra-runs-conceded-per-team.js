const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function extraRunsConceded(year) {
    let matchStart;
    let matchEnd;
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarowMatches) {
        if (datarowMatches.season === String(year)) {
            if (matchStart === undefined) {
                matchStart = +datarowMatches.id;
            } else {
                matchEnd = +datarowMatches.id;
            }
        }
    }).on('end', () => {
        let matchesInSeason = [];
        fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarowDeliveries) {

            if (datarowDeliveries.match_id >= matchStart && datarowDeliveries.match_id <= matchEnd) {
                matchesInSeason.push(datarowDeliveries);
            }

        }).on('end', () => {
            let extraRunsConceded = matchesInSeason.reduce((accu,match)=>{
                if (accu[match.bowling_team] !== undefined){
                    accu[match.bowling_team] += +match.extra_runs;
                }else{
                    accu[match.bowling_team] = +match.extra_runs;
                }
                return accu;
            },{})
            
            fs.writeFile(
                path.join(__dirname, "../public/output/3-extra-runs-conceded-per-team.json"),
                JSON.stringify(extraRunsConceded),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
    })

}

extraRunsConceded(2016);
