const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function bestEconomicalBowlers() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarowDeliveries) {

        if (datarowDeliveries.is_super_over === "1") {
            matches.push(datarowDeliveries);
        }
    })
        .on('end', () => {
            let bowlersList = matches.reduce((accu,match)=>{
                if (accu[match.bowler] === undefined) {
                    accu[match.bowler] = { totalRuns: 0, totalBalls: 0, };
                }
                if (match.extra_runs === "0") {
                    accu[match.bowler].totalRuns += +match.total_runs;
                    accu[match.bowler].totalBalls += 1;
                    accu[match.bowler]["economyOfSuperOver"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                } else {
                    if (match.wide_runs !== "0" || match.noball_runs !== "0") {
                        accu[match.bowler].totalRuns += +match.total_runs - match.penalty_runs;
                        accu[match.bowler]["economyOfSuperOver"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                    } else if (match.bye_runs !== "0" || match.legbye_runs !== "0") {
                        accu[match.bowler].totalBalls += 1;
                        accu[match.bowler]["economyOfSuperOver"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                    }
                }
                return accu;
            },{})
            let bestBowler = Object.entries(bowlersList).sort(function ([firstPlayer, firstPlayerDetails], [secondPlayer, secondPlayerDetails]) {
                return firstPlayerDetails.economyOfSuperOver - secondPlayerDetails.economyOfSuperOver;
            })[0]
            fs.writeFile(
                path.join(__dirname, "../public/output/9-best-economical-bowler-super-over.json"),
                JSON.stringify(bestBowler),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
}

bestEconomicalBowlers();