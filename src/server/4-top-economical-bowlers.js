const fs = require("fs");
const { parse } = require("csv-parse");
const path = require("path");

function topEconomicalBowlers(year,limit) {
    let matchStart;
    let matchEnd;
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarowMatches) {
        if (datarowMatches.season === String(year)) {
            if (matchStart === undefined) {
                matchStart = +datarowMatches.id;
            } else {
                matchEnd = +datarowMatches.id;
            }
        }
    }).on('end', () => {
        let matches = [];
        fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarowDeliveries) {
            if (datarowDeliveries.match_id >= matchStart && datarowDeliveries.match_id <= matchEnd) {
                matches.push(datarowDeliveries);
            }
        }).on('end', () => {
            let result = matches.reduce((accu,match)=>{
                if (accu[match.bowler] === undefined) {
                    accu[match.bowler] = { totalRuns: 0, totalBalls: 0 };
                }
                if (match.extra_runs === "0") {
                    accu[match.bowler].totalRuns += +match.total_runs;
                    accu[match.bowler].totalBalls += 1;
                    accu[match.bowler]["economy"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                } else {
                    if (match.wide_runs !== "0" || match.noball_runs !== "0") {
                        accu[match.bowler].totalRuns += +match.total_runs - match.penalty_runs;
                        accu[match.bowler]["economy"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                    } else if (match.bye_runs !== "0" || match.legbye_runs !== "0") {
                        accu[match.bowler].totalBalls += 1;
                        accu[match.bowler]["economy"] = +(accu[match.bowler]["totalRuns"] / (accu[match.bowler]["totalBalls"] / 6)).toFixed(2);
                    }
                }
                return accu;
            },{})
            let resultList = Object.entries(result).sort(function (first, second) {
                return first[1].economy - second[1].economy;
            }).slice(0, limit).map((player)=>{
                let resultObject = {};
                resultObject[player[0]] = player[1];
                return resultObject;
            });
            fs.writeFile(
                path.join(__dirname, "../public/output/4-top-economical-bowlers.json"),
                JSON.stringify(resultList),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
    })

}

topEconomicalBowlers(2015,10);
