const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");


function wonTossWonMatch() {
    let matches = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        matches.push(datarow);
    })
        .on("end", () => {
            wonTossAndMatch = matches.reduce((accu,match)=>{
                if (match.toss_winner === match.winner) {
                    if (match.toss_winner in accu) {
                        accu[match.toss_winner] += 1;
                    } else {
                        accu[match.toss_winner] = 1;
                    }
                }
                return accu;
            },{})
            fs.writeFile(
                path.join(__dirname, "../public/output/5-won-toss-and-match.json"),
                JSON.stringify(wonTossAndMatch),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}

wonTossWonMatch();
